require 'thor'

require_relative 'version'
require_relative 'kube'


module NodeMonitor
  class Cli < Thor

    class_option 'verbose',  :type => :boolean, :default => false

    # Raising Error will exit with a non-zero exit code
    def self.exit_on_failure?
      true
    end

    # following allows version to be called as a command or a switch
    desc 'version', 'Display version'
    map %w[-v --version] => :version

    def version
      say "monitor #{NodeMonitor::VERSION}"
    end


    desc 'monitor', 'Start monitoring the nodes'
    def monitor
      client = NodeMonitor::K8sClient.new
      client.check_nodes
    end


    no_commands {
      def foo
        say "hello"
      end
    }
  end
end
