require 'kubeclient'

module NodeMonitor

  class K8sClient

    def initialize

      auth_options = {
        bearer_token_file: '/var/run/secrets/kubernetes.io/serviceaccount/token'
      }
      ssl_options = {}
      if File.exist?("/var/run/secrets/kubernetes.io/serviceaccount/ca.crt")
        ssl_options[:ca_file] = "/var/run/secrets/kubernetes.io/serviceaccount/ca.crt"
      end
      @client = Kubeclient::Client.new(
        'https://kubernetes.default.svc',
        'v1',
        auth_options: auth_options,
        ssl_options:  ssl_options
      )
    end

    def check_nodes
      stats = @client.get_nodes[0]["status"]["conditions"]

      stats.each {|s| puts s[:type]}

      # FrequentKubeletRestart                                                                                                                                                                                                                                                    │
      # FrequentDockerRestart                                                                                                                                                                                                                                                     │
      # FrequentContainerdRestart                                                                                                                                                                                                                                                 │
      # KernelDeadlock                                                                                                                                                                                                                                                            │
      # ReadonlyFilesystem                                                                                                                                                                                                                                                        │
      # CorruptDockerOverlay2                                                                                                                                                                                                                                                     │
      # FrequentUnregisterNetDevice                                                                                                                                                                                                                                               │
      # NetworkUnavailable                                                                                                                                                                                                                                                        │
      # MemoryPressure                                                                                                                                                                                                                                                            │
      # DiskPressure                                                                                                                                                                                                                                                              │
      # PIDPressure                                                                                                                                                                                                                                                               │
      # Ready


    end
    
  end
end
