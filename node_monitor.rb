#!/usr/bin/env ruby

require_relative 'node_monitor/cli'
require_relative 'env12f/parse'

# Check for 12 factor variables and create command line for thor
if ARGV.empty?
  ARGV.replace(Env12f::Parse('NODE_MONITOR'))
end

NodeMonitor::Cli.start
