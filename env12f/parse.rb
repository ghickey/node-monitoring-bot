

module Env12f

  def Env12f.Parse (prefix = nil)
    # extract the variables that start with prefix
    vars = ENV.keys.select {|hash| hash.start_with?(prefix) }

    args = []
    vars.each do |v|
      sw = v.gsub(prefix+'_', '')
      val = ENV[prefix+'_'+sw]

      if sw == 'ARGS'
        args << tokenize(val)
      else
        sw = sw.length == 1 ? '-'+sw.downcase : '--'+sw.downcase

        case val
        when /true/i
          args.insert(0, sw)
        when /false/i
        else
          args.insert(0, [sw, val])
        end
      end
    end

    args.flatten!
  end

  def Env12f.tokenize (text)
    text.split(' ')
  end

end
