FROM ruby:2.7-buster
MAINTAINER "Gerard Hickey <ghickey@gitlab.com>"

# throw errors if Gemfile has been modified since Gemfile.lock
#RUN bundle config --global frozen 1

WORKDIR /usr/src/app

COPY Gemfile ./
RUN bundle install

COPY . .

CMD ["./node_monitor.rb"]
